import {Component} from '@angular/core';
import {OperatorComponent} from '../operator/operator.component';
import {OperatorService} from '../operator/operator.service';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'list-operators',
    templateUrl: './list-operators.component.html'
}) 
export class ListOperatorsComponent {

    operators: OperatorComponent[];
    operatorListForm: FormGroup;
    operatorService: OperatorService;
    search: string;
 
    constructor(fb: FormBuilder, operatorService: OperatorService){
        console.log(operatorService);
        this.operatorService = operatorService;
        this.operatorListForm = fb.group({
            search: ['']
        });
    }

    ngOnInit(){
        this.operators = this.operatorService.getOperators();
    }

    remove(operator){
        this.operators = this.operatorService.remove(operator);
    }

}
