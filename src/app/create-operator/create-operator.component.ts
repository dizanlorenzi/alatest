import {Component} from '@angular/core';
import {OperatorComponent} from '../operator/operator.component';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {OperatorService} from '../operator/operator.service';

//Não vou reutilizar em outras aplicações, então vou importar direto no AppModule
@Component({
    moduleId: module.id,
    selector: 'create-operator',
    templateUrl: './create-operator.component.html',
    styleUrls: ['./create-operator.component.css']
})
export class CreateOperatorComponent {

    operator: OperatorComponent = new OperatorComponent;
    operatorForm: FormGroup;
    operatorService: OperatorService;

    constructor(fb: FormBuilder, operatorService: OperatorService){
        this.operatorService = operatorService;
        this.operatorForm = fb.group({
            name: ['', Validators.required],
            prefix: ['', Validators.required],
            price: ['', Validators.required]
        });
    }

    onSubmit(event) {
        event.preventDefault();
        //add to service
        this.addOperator();
    }

    addOperator(){
        this.operatorService.add(this.operator);
        //clear form
        this.operator = new OperatorComponent();   
        this.operatorForm.controls.name.markAsUntouched();
        this.operatorForm.controls.prefix.markAsUntouched();
        this.operatorForm.controls.price.markAsUntouched();
    }

}