"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var operator_component_1 = require('../operator/operator.component');
var forms_1 = require('@angular/forms');
var operator_service_1 = require('../operator/operator.service');
//Não vou reutilizar em outras aplicações, então vou importar direto no AppModule
var CreateOperatorComponent = (function () {
    function CreateOperatorComponent(fb, operatorService) {
        this.operator = new operator_component_1.OperatorComponent;
        this.operatorService = operatorService;
        this.operatorForm = fb.group({
            name: [null, forms_1.Validators.required],
            prefix: [null, forms_1.Validators.required],
            price: [null, forms_1.Validators.required]
        });
    }
    CreateOperatorComponent.prototype.addOperator = function (event) {
        event.preventDefault();
        //add to service
        this.operatorService.add(this.operator);
        //clear form
        this.operator = new operator_component_1.OperatorComponent();
    };
    CreateOperatorComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'create-operator',
            templateUrl: './create-operator.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, operator_service_1.OperatorService])
    ], CreateOperatorComponent);
    return CreateOperatorComponent;
}());
exports.CreateOperatorComponent = CreateOperatorComponent;
//# sourceMappingURL=create-operator.component.js.map