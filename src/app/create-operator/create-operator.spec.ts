import { NO_ERRORS_SCHEMA } from '@angular/core';
import {TestBed, inject, async} from '@angular/core/testing';
import {CreateOperatorComponent} from './create-operator.component';
import {OperatorComponent} from '../operator/operator.component';
import {OperatorService} from '../operator/operator.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

describe('Component: CreateOperatorComponent', () => {
    let fixture, createOperator, element, service;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
          declarations: [CreateOperatorComponent],
          providers: [
            FormBuilder,
            OperatorService
          ],
          schemas: [NO_ERRORS_SCHEMA]
        })
        .compileComponents()
          .then(() => {
            fixture = TestBed.createComponent(CreateOperatorComponent);
            createOperator = fixture.componentInstance;
            element = fixture.nativeElement; 
          });
      }));
        

      beforeEach(inject([OperatorService], s => {
        service = s;
      }));

      // create reusable function for a dry spec.
     const fillForm = (name: string, prefix: number, price: number ) => {
        createOperator.operatorForm.controls['name'].setValue(name);
        createOperator.operatorForm.controls['prefix'].setValue(prefix);
        createOperator.operatorForm.controls['price'].setValue(price);
      };
    
      it('should have default props', async(() => {
        expect(createOperator.operator).toEqual(new OperatorComponent());
      }));


      it('form value should update from form changes', async(() => {
        fillForm('A', 1234, 1.2);
        expect(createOperator.operatorForm.controls['name'].value).toEqual('A');
        expect(createOperator.operatorForm.controls['prefix'].value).toEqual(1234);
        expect(createOperator.operatorForm.controls['price'].value).toEqual(1.2);
      }));

      it('isValid should be false when form is invalid', async(() => {
        fillForm(null, null, null);
        expect(createOperator.operatorForm.valid).toBeFalsy();
      }));

      it('should add operator, ignore duplicate and clear model on submit', async(() => {
        fillForm('B', 4321, 2.1);
        expect(service.getOperators().length).toEqual(0);
        createOperator.addOperator();
        expect(createOperator.operator).toEqual(new OperatorComponent());
        expect(service.getOperators().length).toEqual(1);
        fillForm('B', 4321, 2.1);
        expect(service.getOperators().length).toEqual(1);
        createOperator.addOperator();
        expect(createOperator.operator).toEqual(new OperatorComponent());
        expect(service.getOperators().length).toEqual(1);
      }));

  });

