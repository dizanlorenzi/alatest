import { Pipe, PipeTransform} from '@angular/core';
import {OperatorComponent} from './operator.component';

@Pipe({
    name: 'cheapestOperatorPrice',  
})
export class CheapestOperatorPriceFilter implements PipeTransform{

    transform(operators: OperatorComponent[], search: string): OperatorComponent[]{

        if(typeof search == 'undefined' || search === null){
            search = '';
        }

        //filter only prefixs
        let filteredSearch:string = search.toString().replace(/\D/g,'');

        if(filteredSearch.length > 0){   

            let filteredOperators:OperatorComponent[] = operators
                                    .filter((operator) => {
                                        let prefix = operator.prefix.toString();
                                        //get first characters that match the prefix size to do a proper search
                                        let searchText = filteredSearch.substring(0, prefix.length);
                                        return searchText.indexOf(prefix) > -1  
                                    })
                                    .reduce((result:OperatorComponent[], currentOperator:OperatorComponent) => {

                                        //find current operator in result
                                        let operator = result.find(operator => operator.name == currentOperator.name);

                                        //this operator already has one entry
                                        if(operator){   
                                                             
                                            //check if the length is bigger
                                            if(currentOperator.prefix.toString().length > operator.prefix.toString().length){
                                                let index = result.indexOf(operator);
                                                result[index] = currentOperator;  
                                            
                                            } //check if the length is the same take the cheapest
                                            else if(currentOperator.prefix.toString().length == operator.prefix.toString().length ){
                                                
                                                if(operator.price > currentOperator.price){
                                                    let index = result.indexOf(operator);
                                                    result[index] = currentOperator;
                                                }      
                                            }

                                        } else {
                                            //first entry of the operator (currentOperator) is being added
                                            result.push(currentOperator);
                                        }

                                        return result;

                                    }, Array<OperatorComponent>());

            console.log("filtered operators", filteredOperators);

            return filteredOperators;
        } else {

            //return entire list if search criteria was not met
            console.log("all operators", operators);
            return operators;
        }

    }

}