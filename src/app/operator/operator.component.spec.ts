import {TestBed, inject, async} from '@angular/core/testing';
import {OperatorComponent} from './operator.component';

describe('Component: OperatorComponent', () => {
    let fixture, operator, element;
    
    //setup
    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ OperatorComponent ]
      });
  
      fixture = TestBed.createComponent(OperatorComponent);
      operator = fixture.componentInstance;  // to access properties and methods
      element = fixture.nativeElement;      // to access DOM element
    });
    
    //specs
    it('should render operator component properties', async() => {
            operator.name = 'A';
            operator.prefix = 1234;
            operator.price = 1.1;
            //trigger change detection
            fixture.detectChanges();
            fixture.whenStable().then(() => { 
              expect(element.querySelector('.operator-name').innerText).toContain('A');
              expect(element.querySelector('.operator-prefix').innerText).toContain('1234');
              expect(element.querySelector('.operator-price').innerText).toContain('1.1');
            });
    });
    
  });

