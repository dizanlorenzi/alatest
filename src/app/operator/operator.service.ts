import { Injectable } from '@angular/core';
import {OperatorComponent} from './operator.component';

@Injectable()
export class OperatorService {

    operators: OperatorComponent[] = [];

    add(addedOperator: OperatorComponent): OperatorComponent  {

        //find equal entry
        let operatorExists = this.operators
            .filter(operator => operator.prefix === addedOperator.prefix 
            && operator.name === addedOperator.name
            && operator.price === addedOperator.price);

        //add only if there is no entry like this
        if(operatorExists.length == 0){
            this.operators.push(addedOperator);
            console.log("added operator", addedOperator);
        } else {
            console.log("already exists and was ignored", addedOperator);
        }

        return addedOperator;
    }

    getOperators(): OperatorComponent[] {
        return this.operators;
    }

   remove(removedOperator: OperatorComponent): OperatorComponent[]  { 

        //filter operators without the same properties, therefore removing the wanted operator
        let operatorsWithoutRemoved = this.operators
            .filter(operator => !(operator.prefix === removedOperator.prefix 
            && operator.name === removedOperator.name
            && operator.price === removedOperator.price));

        console.log("operators without removed", operatorsWithoutRemoved);

        return this.operators = operatorsWithoutRemoved;
    }


}