import {TestBed, inject} from '@angular/core/testing';
import {OperatorService} from './operator.service';
import {OperatorComponent} from './operator.component';

describe('Service: OperatorService', () => {
    let service;
  
    beforeEach(() => TestBed.configureTestingModule({
      providers: [ OperatorService ]
    }));
  
    beforeEach(inject([OperatorService], s => {
      service = s;
    }));
  
    it('should return an empty list of operators', () => {
        let operators = service.getOperators();
        expect(operators.length).toEqual(0);
    });

    it('should add one operator', () => {
        service.add(new OperatorComponent());
        let operators = service.getOperators();
        expect(operators.length).toEqual(1);
    });


    it('should remove one operator', () => {
        service.remove(new OperatorComponent());
        let operators = service.getOperators();
        expect(operators.length).toEqual(0);
    });

});