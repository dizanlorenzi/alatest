"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var operator_component_1 = require('./operator.component');
var operator_pipes_1 = require('./operator.pipes');
var operator_service_1 = require('./operator.service');
var OperatorModule = (function () {
    function OperatorModule() {
    }
    OperatorModule = __decorate([
        core_1.NgModule({
            declarations: [operator_component_1.OperatorComponent, operator_pipes_1.CheapestOperatorPriceFilter],
            exports: [operator_component_1.OperatorComponent, operator_pipes_1.CheapestOperatorPriceFilter],
            providers: [operator_service_1.OperatorService]
        }), 
        __metadata('design:paramtypes', [])
    ], OperatorModule);
    return OperatorModule;
}());
exports.OperatorModule = OperatorModule;
//# sourceMappingURL=operator.module.js.map