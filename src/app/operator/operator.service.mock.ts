import { Injectable } from '@angular/core';
import {OperatorComponent} from './operator.component';

@Injectable()
export class OperatorServiceMock {

    operators: OperatorComponent[] = [];

    constructor(){

        let fakeOperators = [
            {name: 'A', prefix: 1, price: 0.9},
            {name: 'A', prefix: 268, price: 5.1},
            {name: 'A', prefix: 46, price: 0.17},
            {name: 'A', prefix: 4620, price: 0.0},
            {name: 'A', prefix: 468, price: 0.15},
            {name: 'A', prefix: 4631, price: 0.15},
            {name: 'A', prefix: 4673, price: 0.9},
            {name: 'A', prefix: 46732, price: 1.1},
            {name: 'B', prefix: 1, price: 0.92},
            {name: 'B', prefix: 44, price: 0.5},
            {name: 'B', prefix: 46, price: 0.2},
            {name: 'B', prefix: 467, price: 1.0},
            {name: 'B', prefix: 48, price: 1.2}
        ];

        this.operators = fakeOperators.map(fakeOperator => {
            let newOperator = new OperatorComponent();
            newOperator.name = fakeOperator.name;
            newOperator.prefix = fakeOperator.prefix;
            newOperator.price = fakeOperator.price;
            return newOperator;
        });
    }

    add(addedOperator: OperatorComponent): OperatorComponent  {

        //find equal entry
        let operatorExists = this.operators
            .filter(operator => operator.prefix === addedOperator.prefix 
            && operator.name === addedOperator.name
            && operator.price === addedOperator.price);

        //add only if there is no entry like this
        if(operatorExists.length == 0){
            this.operators.push(addedOperator);
            console.log("added operator", addedOperator);
        } else {
            console.log("already exists and was ignored", addedOperator);
        }

        return addedOperator;
    }

    getOperators(): OperatorComponent[] {
        return this.operators;
    }

   remove(removedOperator: OperatorComponent): OperatorComponent[]  { 

        //filter operators without the same properties, therefore removing the wanted operator
        let operatorsWithoutRemoved = this.operators
            .filter(operator => !(operator.prefix === removedOperator.prefix 
            && operator.name === removedOperator.name
            && operator.price === removedOperator.price));

        console.log("operators without removed", operatorsWithoutRemoved);

        return this.operators = operatorsWithoutRemoved;
    }


}