"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var OperatorService = (function () {
    function OperatorService() {
        this.operators = [];
    }
    OperatorService.prototype.add = function (addedOperator) {
        //find equal entry
        var operatorExists = this.operators
            .filter(function (operator) { return operator.prefix === addedOperator.prefix
            && operator.name === addedOperator.name
            && operator.price === addedOperator.price; });
        //add only if there is no entry like this
        if (operatorExists.length == 0) {
            this.operators.push(addedOperator);
            console.log("added operator", addedOperator);
        }
        else {
            console.log("already exists and was ignored", addedOperator);
        }
        return addedOperator;
    };
    OperatorService.prototype.getOperators = function () {
        return this.operators;
    };
    OperatorService.prototype.remove = function (removedOperator) {
        //filter operators without the same properties, therefore removing the wanted operator
        var operatorsWithoutRemoved = this.operators
            .filter(function (operator) { return !(operator.prefix === removedOperator.prefix
            && operator.name === removedOperator.name
            && operator.price === removedOperator.price); });
        console.log("operators without removed", operatorsWithoutRemoved);
        return this.operators = operatorsWithoutRemoved;
    };
    OperatorService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], OperatorService);
    return OperatorService;
}());
exports.OperatorService = OperatorService;
//# sourceMappingURL=operator.service.js.map