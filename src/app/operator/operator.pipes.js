"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var CheapestOperatorPriceFilter = (function () {
    function CheapestOperatorPriceFilter() {
    }
    CheapestOperatorPriceFilter.prototype.transform = function (operators, search) {
        //filter only prefixs
        var filteredSearch = search.replace(/\D/g, '');
        if (filteredSearch.length > 0) {
            var filteredOperators = operators
                .filter(function (operator) { return filteredSearch.indexOf(operator.prefix.toString()) > -1; })
                .reduce(function (result, currentOperator) {
                //find current operator in result
                var operator = result.find(function (operator) { return operator.name == currentOperator.name; });
                //this operator already has one entry
                if (operator) {
                    //check if the length is bigger
                    if (currentOperator.prefix.toString().length > operator.prefix.toString().length) {
                        var index = result.indexOf(operator);
                        result[index] = currentOperator;
                    } //check if the length is the same take the cheapest
                    else if (currentOperator.prefix.toString().length == operator.prefix.toString().length) {
                        if (operator.price > currentOperator.price) {
                            var index = result.indexOf(operator);
                            result[index] = currentOperator;
                        }
                    }
                }
                else {
                    //first entry of the operator (currentOperator) is being added
                    result.push(currentOperator);
                }
                return result;
            }, Array());
            console.log("filtered operators", filteredOperators);
            return filteredOperators;
        }
        else {
            //return entire list if search criteria was not met
            return operators;
        }
    };
    CheapestOperatorPriceFilter = __decorate([
        core_1.Pipe({
            name: 'cheapestOperatorPrice',
        }), 
        __metadata('design:paramtypes', [])
    ], CheapestOperatorPriceFilter);
    return CheapestOperatorPriceFilter;
}());
exports.CheapestOperatorPriceFilter = CheapestOperatorPriceFilter;
//# sourceMappingURL=operator.pipes.js.map