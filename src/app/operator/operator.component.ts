import {Component, Input} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'operator',
    templateUrl: './operator.component.html',
    styleUrls: ['./operator.component.css']
})
export class OperatorComponent{

    @Input() name: string = null;
    @Input() prefix: number = null;
    @Input() price: number = null;
}
