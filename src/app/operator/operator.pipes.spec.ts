import {TestBed, inject} from '@angular/core/testing';
import {CheapestOperatorPriceFilter} from './operator.pipes';
import {OperatorComponent} from './operator.component';
import {OperatorService} from './operator.service';

describe('Pipe: CheapestOperatorPriceFilter', () => {
    let pipe;
    
    //setup
    beforeEach(() => TestBed.configureTestingModule({
        providers: [ CheapestOperatorPriceFilter ]
      }));
    
    beforeEach(inject([CheapestOperatorPriceFilter], p => {
      pipe = p;
    }));
    
    //specs

    it('should work with null', () => {
      expect(pipe.transform(Array<OperatorComponent>(), null)).toEqual(Array<OperatorComponent>());
    });

    it('should work with undefined', () => {
      expect(pipe.transform(Array<OperatorComponent>(), undefined)).toEqual(Array<OperatorComponent>());
    });

    it('should work with empty string', () => {
      expect(pipe.transform(Array<OperatorComponent>(),'')).toEqual(Array<OperatorComponent>());
    });

    it('should work with numbers', () => {
      expect(pipe.transform(Array<OperatorComponent>(),1234)).toEqual(Array<OperatorComponent>());
    });

    it('should work with strings', () => {
      expect(pipe.transform(Array<OperatorComponent>(),'1234')).toEqual(Array<OperatorComponent>());
    });

    it('should filter special characters', () => {
      expect(pipe.transform(Array<OperatorComponent>(),'+46-73-212345')).toEqual(Array<OperatorComponent>());
    });
    
    it('should filter letters', () => {
      expect(pipe.transform(Array<OperatorComponent>(),'abc+46-73-212345abc')).toEqual(Array<OperatorComponent>());
    });

  });