import {NgModule} from '@angular/core';
import {OperatorComponent} from './operator.component';
import {CheapestOperatorPriceFilter} from './operator.pipes';
import { OperatorService } from './operator.service';

@NgModule({
    declarations: [OperatorComponent, CheapestOperatorPriceFilter],
    exports: [OperatorComponent, CheapestOperatorPriceFilter],
    providers: [OperatorService]
})
export class OperatorModule {}