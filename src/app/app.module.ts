import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import {AppComponent} from './app.component';
import {OperatorModule} from './operator/operator.module';
import {ListOperatorsComponent} from './list-operators/list-operators.component';
import {CreateOperatorComponent} from './create-operator/create-operator.component';
import {routing} from './app.routes';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        BrowserModule, 
        OperatorModule, 
        HttpModule, 
        routing,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        AppComponent, 
        ListOperatorsComponent, 
        CreateOperatorComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}