import {RouterModule, Routes } from '@angular/router';
import {ListOperatorsComponent} from './list-operators/list-operators.component';
import {CreateOperatorComponent} from './create-operator/create-operator.component';

const appRoutes: Routes = [
    {path: '', component: ListOperatorsComponent},
    {path: 'create', component: CreateOperatorComponent},
    {path: '**', component: ListOperatorsComponent}
];

export const routing = RouterModule.forRoot(appRoutes);